# Swinburne Harvard Reference LaTeX Style

## NOTE:
This project is still very much a work in progress, some referenes styles may be incorrect, I and anyone affliated with this project does not take any responsibility for loss of marks or any university penalties that are applied to you for using this style.

## Dependencies
- texLive
- Natbib

## Installation
Download and put inside your project file and include it using these commands
``` 
\begin{document}
. 
.
\bibliographystyle{swinHarvard}
.
.
\end{document}
```

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Testing
All testing is done on an arch-based linux distro.

## Contributing
If you want to contribute a change is done to the bst file and a screenshot of how the reference looks with the change compared to the swinburne example.

See here for example: https://www.swinburne.edu.au/library/search/referencing-guides/harvard-style-guide/
## License
Fully open source, please fork and improve if you can.

## Project status
This project is updated when I feel like it or need it.
